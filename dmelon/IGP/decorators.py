#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
Created on Tue Jan 15 09:26:22 2019
Author: Gerardo A. Rivera Tello
Email: grivera@igp.gob.pe
-----
Copyright (c) 2018 Instituto Geofisico del Peru
-----
"""

import time

def time_func(func):
    def timed(*args, **kw):
        time1=time.time()
        result = func(*args,**kw)
        time2=time.time()
        print('[{}] Total elapsed time: {}'.format(func.__name__,time2-time1))
        return result
    return timed