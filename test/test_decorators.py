#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
Created on Wed Jan 16 12:02:27 2019
Author: Gerardo A. Rivera Tello
Email: grivera@igp.gob.pe
-----
Copyright (c) 2018 Instituto Geofisico del Peru
-----
"""

from dmelon.IGP import decorators
from time import sleep

@decorators.time_func
def test_time(n=3):
    for i in range(n):
        sleep(i+1)
    print("Slept for {} seconds".format(n*(n+1)/2))